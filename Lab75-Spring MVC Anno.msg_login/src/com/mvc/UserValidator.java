package com.mvc;




import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;


@Component

public class UserValidator implements Validator  {

	public boolean supports(Class clazz) {
		return User.class.equals(clazz);
	}
	
	public void validate(Object command, Errors errors) {
	User user=(User) command;
	
	if(user.getUsername()==null|| user.getUsername().length()==0) {	
	errors.rejectValue("username","error.username.required");
	}

	
	if(user.getPassword()==null|| user.getPassword().length()==0) {	
	errors.rejectValue("password","error.password.required");
	
// without this also will Run.  "errors.password.required",null remainings r need.  
	
}

	}
              // without this also will run 
	
}
