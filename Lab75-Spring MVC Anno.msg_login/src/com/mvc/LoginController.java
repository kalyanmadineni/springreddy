package com.mvc;

import java.util.Map;

import javax.servlet.ServletException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

@Controller
@SessionAttributes
public class LoginController {

    	@Autowired
        private  UserValidator  usertValidator;
    
    @RequestMapping(value="/verifyUser" , method=RequestMethod.POST)

    public String egisterStudent(@ModelAttribute("user1")User user,BindingResult result)
    throws ServletException{
    	
    	usertValidator.validate( user, result);
    	
    	if(result.hasErrors()) {
    		System.out.println(result.getErrorCount());
    		return "login";
    	}

    	//	System.out.println(user.getUsername());
    		//System.out.println(user.getPassword());
    		
    	
    	
        String un=user.getUsername();
    	String pw=user.getPassword();
    	System.out.println(un);
    	System.out.println(pw);
    

    	return "home";

    }
    /*
	
	@RequestMapping(value="/verifyUser" , method=RequestMethod.POST)

public String loginform(@ModelAttribute("user1")User user) {
	
	
	String um=user.getUsername();
	String ps=user.getPassword();
	System.out.println(um);
	System.out.println(ps);
	return "home";
}
     	*/

    @RequestMapping("/login")
    public String showLoginForm(Map<String, User>  model)throws ServletException{
    	System.out.println("showLoginForm");
    	User user=new User();
    	user.setUsername("raja");
    //	user.setPassword("1223");   object set not accept password
    	model.put("user1",user);
    	return "login";
    	
    	
    }
}
